package BibliotecaApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {

    Carte book;
    Carte book1;

    @Before
    public void setUp() throws Exception {

        book = new Carte();


        book.setTitlu("Poems");
        book.setEditura("Adevarul");

        System.out.println("Before test!");
    }

    @After
    public void tearDown() throws Exception {

        book = null;

        System.out.println("Dupa test!");
    }
    //@Ignore ("Ignored test...")
    @Test
    public void getTitlu() {

        assertEquals("titlu = Poems", "Poems", book.getTitlu());
    }


    @Test
    public void setTitlu() {

        book.setTitlu("Ben Hur");

        assertEquals("titlu = Ben Hur", "Ben Hur", book.getTitlu());
    }

    @BeforeClass
    public static void setup() throws Exception {



        System.out.println("BeforeClass!");
    }

    @AfterClass
    public static void getup() throws Exception {



        System.out.println("AfterClass!");
    }

    @Test (timeout = 100)
    public void getEditura() {

        try {
            Thread.sleep(99);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals("Editura = Adevarul", "Adevarul", book.getEditura());


    }
    @Test (expected = NullPointerException.class)
    public void testConstructor(){

        assert book1 == null;
        throw new NullPointerException();


    }
    @Test
    public void setAnAparitie(){

        book.setAnAparitie("2000");
        assertEquals("An = 2000", "2000", book.getAnAparitie());
    }





}