package BibliotecaApp.control;


import BibliotecaApp.model.Carte;
import BibliotecaApp.repository.repoInterfaces.CartiRepoInterface;
import BibliotecaApp.util.Validator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class BibliotecaCtrlTest_blackBox {
    Carte book;
    BibliotecaCtrl bibl;
    Validator validator;
    CartiRepoInterface cartiInterface;
    List<String> autoriList = new ArrayList<>();
    List<String> cuvinteCheieList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        cartiInterface = new CartiRepoInterface() {
            @Override
            public void adaugaCarte(Carte c) {

            }

            @Override
            public void modificaCarte(Carte nou, Carte vechi) {

            }

            @Override
            public void stergeCarte(Carte c) {

            }

            @Override
            public List<Carte> cautaCarte(String autor) {
                return null;
            }

            @Override
            public List<Carte> getCarti() {
                return null;
            }

            @Override
            public List<Carte> getCartiOrdonateDinAnul(String an) {
                return null;
            }
        };
        book = new Carte();
        bibl = new BibliotecaCtrl(cartiInterface);
        System.out.println("@Before ends here!");
    }

    @After
    public void tearDown() throws Exception {
        book = null;
        bibl = null;
        System.out.println("@After completed");
    }

    @Test
    public void addCarte1() throws Exception {
        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura("Adev1rul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test(expected = Exception.class)
    public void addCarte03() throws Exception {
        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "drama1";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura("Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test (expected = Exception.class)
    public void addCarte02() throws Exception {
        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura(" Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test
    public void addCarte4() throws Exception {

        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura(" Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test
    public void addCarte5() throws Exception {

        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura(" Adevarul ");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test(expected = Exception.class)
    public void addCarte6() throws Exception {

        book = new Carte();
        String autori1 = "";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura("Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test(expected = Exception.class)
    public void addCarte7() throws Exception {

        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");
        book.setAutori(autoriList);
        book.setAnAparitie("douamii");
        book.setEditura("Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test(expected = Exception.class)
    public void addCarte8() throws Exception {
        book = new Carte();
        String autori1 = "Goethe";
        String autori2 = "GoetheM";
        String cuvinte1 = "drama";
        autoriList.add(autori1);
        autoriList.add(autori2);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("Faust");

        book.setAutori(autoriList);
        book.setAnAparitie("douamii");
        book.setEditura("Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }

    @Test(expected = Exception.class)

    public void addCarte09() throws Exception {
        book = new Carte();
        String autori1 = "Goethe";
        String cuvinte1 = "";
        autoriList.add(autori1);
        cuvinteCheieList.add(cuvinte1);
        book.setTitlu("");
        book.setAutori(autoriList);
        book.setAnAparitie("1999");
        book.setEditura(" Adevarul");
        book.setCuvinteCheie(cuvinteCheieList);
        bibl.adaugaCarte(book);
    }
}