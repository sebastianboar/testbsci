package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.Before;
import org.junit.Test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CartiRepoMockTest_whiteBox {

    private Carte c1, c2, c3;
    CartiRepoMock cartiRepoMock;

    @Before
    public void setUp() throws Exception {

        c1 = new Carte();
        c1.setTitlu("Harry Potter");
        c1.setAutori(Arrays.asList("MX"));
        c1.setAnAparitie("2002");
        c1.setEditura("Adevarul");
        c1.setCuvinteCheie(Arrays.asList("Fantasy","Autumn"));
        c2 = new Carte();
        c2.setTitlu("Lord of rings");
        c2.setAutori(Arrays.asList("MY"));
        c2.setAnAparitie("1545");
        c2.setEditura("Adevarul");
        c2.setCuvinteCheie(Arrays.asList("Fantary","History"));

        c3 = new Carte();
        c3.setTitlu("3 man");
        c3.setAutori(Arrays.asList("Unknwn"));
        c3.setAnAparitie("1555");
        c3.setEditura("Adevarul");
        c3.setCuvinteCheie(Arrays.asList("unknown","emptyness"));
        cartiRepoMock = new CartiRepoMock();


    }

    @After
    public void tearDown() throws Exception {

        c1 = c2 = c3 = null;
        cartiRepoMock = null;
    }

    @Test
    public void cautaCarteBibliotecaFaraCarti() {

        assertEquals("Carte in biblioteca fara carti",0,cartiRepoMock.cautaCarte("Balboa").size());
        System.out.println("Repo gol verificat cu succes!");

    }
    @Test
    public void cautaCarteBibliotecaCuAutor() {
        cartiRepoMock.adaugaCarte(c1);
        cartiRepoMock.adaugaCarte(c2);

        assertEquals("Cautam autor existent, fara succes",1,cartiRepoMock.cautaCarte("MX").size());
        System.out.println("Autor existent gasit!");

    }

    @Test
    public void cautaCarteBibliotecaFaraAutor() {
        cartiRepoMock.adaugaCarte(c3);

        assertEquals("Cautam autor inexistent, fara succes",0,cartiRepoMock.cautaCarte("Cioran").size());
        System.out.println("Autor inexistent, succes!");

    }
}